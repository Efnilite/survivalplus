package tk.shanebee.survival.listeners.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.shanebee.survival.Survival;
import tk.shanebee.survival.config.Config;

public class RecoveryListener implements Listener {

    @EventHandler
    public void fall(EntityDamageEvent e) { // Recovery
        if (e.getCause() == EntityDamageEvent.DamageCause.FALL && e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            double fall = player.getFallDistance();

            Config config = Survival.getInstance().getSurvivalConfig();

            if (fall >= config.RECOVERY_DISTANCE) {
                player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) (config.RECOVERY_TIME * 20),
                        (int) config.RECOVERY_AMPLIFIER, false, false));
            }
        }
    }
}