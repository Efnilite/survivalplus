package tk.shanebee.survival.listeners.player;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import tk.shanebee.survival.Survival;

public class FallListener implements Listener {

    @EventHandler
    public void fall(EntityDamageEvent e) { // RealismFall
        if (e.getCause() == EntityDamageEvent.DamageCause.FALL && e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            double fall = player.getFallDistance();
            double distance = Survival.getInstance().getSurvivalConfig().FALL_DAMAGE_DISTANCE;

            if (fall >= distance) {
                e.setDamage(distance * fall);
            }
        }
    }
}